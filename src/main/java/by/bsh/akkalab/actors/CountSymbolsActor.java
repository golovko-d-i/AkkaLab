package by.bsh.akkalab.actors;

import by.bsh.akkalab.comand.FilePartCommand;
import by.bsh.akkalab.event.NewDataEvent;

import java.util.HashMap;

/**
 * Created by bsh on 06.12.15.
 */
public class CountSymbolsActor extends AbsStopSelfActor {

    @Override
    public void onReceive(Object message) throws Exception {

        if (message instanceof FilePartCommand) {

            HashMap<Character, Long> result = new HashMap<>();

            String str = ((FilePartCommand) message).getString();
            for (char ch : str.toCharArray()) {
                result.put(ch, result.get(ch) == null ? 1 : result.get(ch) + 1);
            }

            getContext().system().eventStream().publish(new NewDataEvent(result, ((FilePartCommand) message).getFile()));
            stop();

            log.debug("All characters are counted, captain!");

        } else {
            log.debug("Message isn't a string. Let Kracken eat it!");
        }

    }

}
