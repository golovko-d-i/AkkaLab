package by.bsh.akkalab.actors;

import akka.actor.ActorRef;
import akka.actor.Props;
import by.bsh.akkalab.comand.FilePartCommand;
import by.bsh.akkalab.comand.ReadFilePartCommand;

import java.io.FileInputStream;

/**
 * Created by bsh on 06.12.15.
 */
public class ReadFileActor extends AbsStopSelfActor {

    @Override
    public void onReceive(Object message) throws Exception {

        if (message instanceof ReadFilePartCommand) {

            FileInputStream inputStream = new FileInputStream(((ReadFilePartCommand) message).getFile());

            byte[] bytes = new byte[((ReadFilePartCommand) message).getBytesCount()];
            inputStream.skip(((ReadFilePartCommand) message).getOffset());
            inputStream.read(bytes);

            ActorRef counter = getContext().actorOf(Props.create(CountSymbolsActor.class));
            counter.tell(new FilePartCommand(((ReadFilePartCommand) message).getFile(), new String(bytes)), getSelf());

            log.debug("%d bytes from file %s red with %d offset", ((ReadFilePartCommand) message).getBytesCount(),
                    ((ReadFilePartCommand) message).getFile().getName(), ((ReadFilePartCommand) message).getOffset());
            stop();

        } else {
            log.debug("Message isn't a ReadFilePartCommand! Send to feed the sharks!");
        }

    }

}
