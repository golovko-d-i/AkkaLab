package by.bsh.akkalab.actors;

import akka.actor.ActorRef;
import akka.actor.Props;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Created by bsh on 06.12.15.
 */
public class SupervisorActor extends AbsStopSelfActor {

    @Override
    public void onReceive(Object message) throws Exception {

        if (message instanceof File) {

            File rootFolder = (File) message;

            if (!rootFolder.exists() || !rootFolder.isDirectory()) {
                log.debug("Root folder isn't exists or is not a directory! WTF?");
                return;
            }

            // get txt files and send it to file reader actors
            for (File file : rootFolder.listFiles(new TextFileFilter())) {
                ActorRef readFileActor = getContext().actorOf(Props.create(FileSupervisorActor.class));
                readFileActor.tell(file, getSelf());
            }

            log.debug("All files are sent. Yo-ho-ho :)");
            stop();

        } else {
            log.debug("Yo-ho-ho");
        }

    }

    private static class TextFileFilter implements FilenameFilter {

        @Override
        public boolean accept(File dir, String name) {
            return !(name == null || name.isEmpty()) && name.endsWith(".txt");
        }
    }
}
