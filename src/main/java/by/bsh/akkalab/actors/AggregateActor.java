package by.bsh.akkalab.actors;

import by.bsh.akkalab.event.FilePartsCountEvent;
import by.bsh.akkalab.event.NewDataEvent;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Created by bsh on 06.12.15.
 */
public class AggregateActor extends AbsStopSelfActor {

    private final Map<File, Long> calculatedPartsCount = new HashMap<>();
    private final Map<File, Long> partsCount = new HashMap<>();

    private final Map<File, TreeMap<Character, Long>> result = new HashMap<>();

    @Override
    public void onReceive(Object message) throws Exception {

        if (message instanceof FilePartsCountEvent) {

            calculatedPartsCount.put(((FilePartsCountEvent) message).getFile(), 0L);
            partsCount.put(((FilePartsCountEvent) message).getFile(), ((FilePartsCountEvent) message).getPartsCount());

            log.debug("New FilePartCountEvent for file %s with %d parts count",
                    ((FilePartsCountEvent) message).getFile().getName(), ((FilePartsCountEvent) message).getPartsCount());

        } else if (message instanceof NewDataEvent) {

            long count = calculatedPartsCount.get(((NewDataEvent) message).getFile());
            calculatedPartsCount.put(((NewDataEvent) message).getFile(), ++count); // java is not kotlin, java is not kotlin

            // all file parts are aggregated, remove counters
            if (count == partsCount.get(((NewDataEvent) message).getFile())) {
                partsCount.remove(((NewDataEvent) message).getFile());
                calculatedPartsCount.remove(((NewDataEvent) message).getFile());
            }

            final TreeMap<Character, Long> child;

            if (result.get(((NewDataEvent) message).getFile()) == null) {
                child = new TreeMap<>();
                result.put(((NewDataEvent) message).getFile(), child);
            } else {
                child = result.get(((NewDataEvent) message).getFile());
            }

            // hmmmm
            ((NewDataEvent) message).getResult().forEach((rootKey, rootVal) ->
                    child.compute(rootKey, (key, val) -> (val == null) ? rootVal : val + rootVal)
            );

            log.debug("Add new calculated values for file %s", ((NewDataEvent) message).getFile().getName());

        } else {
            log.debug("This message is not for me");
        }

        // calculation complete
        if (partsCount.isEmpty()) {

            // pfff. print each entry
            result.forEach((key, val) ->
                    System.out.println(
                            String.format("%s\t[%s]", key.getName(),
                                    val.entrySet().stream()
                                            .map(e -> String.format("%s = %d", e.getKey(), e.getValue()))
                                            .collect(Collectors.joining(",\t"))))
            );

            log.debug("Calculation complete!");
            getContext().system().terminate();

        }

    }


}
