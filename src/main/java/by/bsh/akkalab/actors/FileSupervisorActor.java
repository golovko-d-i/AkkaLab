package by.bsh.akkalab.actors;

import akka.actor.ActorRef;
import akka.actor.Props;
import by.bsh.akkalab.comand.ReadFilePartCommand;
import by.bsh.akkalab.event.FilePartsCountEvent;

import java.io.File;

/**
 * Created by bsh on 15.12.15.
 */
public class FileSupervisorActor extends AbsStopSelfActor {

    @Override
    public void onReceive(Object message) throws Exception {

        if (message instanceof File) {
            File file = (File) message;

            long offset = 0;
            long partsCount = file.length() / Constants.BYTES_COUNT;

            if (partsCount * Constants.BYTES_COUNT == 0) {
                partsCount = 1;
            } else if (partsCount * Constants.BYTES_COUNT < file.length()) {
                partsCount++;
            }

            getContext().system().eventStream().publish(new FilePartsCountEvent(partsCount, file));

            while (offset < file.length()) {

                int bytesCount = offset + Constants.BYTES_COUNT < file.length() ? Constants.BYTES_COUNT : (int) (file.length() - offset);

                ActorRef readFile = getContext().system().actorOf(Props.create(ReadFileActor.class));
                readFile.tell(new ReadFilePartCommand(file, offset, bytesCount), getSelf());

                offset += bytesCount;
            }

            log.debug("Messages for file %s are sent", file.getName());
            stop();

        } else {
            log.debug("Message isn't a file");
        }

    }
}
