package by.bsh.akkalab.actors;

/**
 * Created by bsh on 15.12.15.
 */
public abstract class AbsStopSelfActor extends AbsLoggerActor {

    protected void stop() {
        getContext().stop(getSelf());
    }

}
