package by.bsh.akkalab.actors;

import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

/**
 * Created by bsh on 15.12.15.
 */
abstract class AbsLoggerActor extends UntypedActor {

    protected LoggingAdapter log = Logging.getLogger(getContext().system(), this);

}
