package by.bsh.akkalab.event;

import java.io.File;

/**
 * Created by bsh on 15.12.15.
 */
public class FilePartsCountEvent {

    private long partsCount;
    private File file;

    public FilePartsCountEvent(long partsCount, File file) {
        this.partsCount = partsCount;
        this.file = file;
    }

    public long getPartsCount() {
        return partsCount;
    }

    public File getFile() {
        return file;
    }
}
