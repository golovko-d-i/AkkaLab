package by.bsh.akkalab.event;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by bsh on 06.12.15.
 */
public class NewDataEvent implements Serializable {

    private static final long serialVersionUID = -7437952037399975899L;

    private final HashMap<Character, Long> result;
    private final File file;

    public NewDataEvent(HashMap<Character, Long> result, File file) {
        this.result = result;
        this.file = file;
    }

    public HashMap<Character, Long> getResult() {
        return result;
    }

    public File getFile() {
        return file;
    }
}
