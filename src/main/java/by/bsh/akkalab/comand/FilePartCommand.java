package by.bsh.akkalab.comand;

import java.io.File;
import java.io.Serializable;

/**
 * Created by bsh on 06.12.15.
 */
public class FilePartCommand implements Serializable {
    private static final long serialVersionUID = 482726870168528868L;

    private final String string;
    private final File file;

    public FilePartCommand(File file, String string) {
        this.string = string;
        this.file = file;
    }

    public String getString() {
        return string;
    }

    public File getFile() {
        return file;
    }
}
