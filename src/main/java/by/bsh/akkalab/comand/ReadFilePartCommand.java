package by.bsh.akkalab.comand;

import java.io.File;
import java.io.Serializable;

/**
 * Created by bsh on 06.12.15.
 */
public class ReadFilePartCommand implements Serializable {
    private static final long serialVersionUID = 2677257910235505056L;

    private final File file;
    private final long offset;
    private final int bytesCount;

    public ReadFilePartCommand(File file, long offset, int bytesCount) {
        this.file = file;
        this.offset = offset;
        this.bytesCount = bytesCount;
    }

    public File getFile() {
        return file;
    }

    public long getOffset() {
        return offset;
    }

    public int getBytesCount() {
        return bytesCount;
    }
}
