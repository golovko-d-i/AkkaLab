package by.bsh.akkalab;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import by.bsh.akkalab.actors.AggregateActor;
import by.bsh.akkalab.actors.SupervisorActor;
import by.bsh.akkalab.event.FilePartsCountEvent;
import by.bsh.akkalab.event.NewDataEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * Created by bsh on 06.12.15.
 */
public class Main {

    public static final Logger log = LoggerFactory.getLogger(System.class);

    public static void main(String[] args) {

        if (args.length > 0) {

            final ActorSystem actorSystem = ActorSystem.create("event-system");

            final ActorRef aggregator = actorSystem.actorOf(Props.create(AggregateActor.class));
            actorSystem.eventStream().subscribe(aggregator, NewDataEvent.class);
            actorSystem.eventStream().subscribe(aggregator, FilePartsCountEvent.class);

//            File folder = new File("/Users/bsh/IdeaProjects/akkalab/text");
            File folder = new File(args[0]);

            ActorRef supervisor = actorSystem.actorOf(Props.create(SupervisorActor.class));
            supervisor.tell(folder, null);

            log.debug("Await for work ends");
            actorSystem.awaitTermination();
        } else {
            System.out.println("Define path to folder as a first command line parameter");
        }
    }
}
