BSTU akka library example
-------------------------

For run type in terminal

	./gradlew run $folder_with_txt_files

on Mac/Linux or 

	gradlew run %FOLDER_WITH_TXT_FILES%

on Windows

### Test run
For test run execute gradle task `testRun`. This task automatically select folder `text` in project folder with sample resources.

Mac/Linux:

	./gradlew testRun
	
Windows:

	gradlew testRun
	
Java 8 is required.